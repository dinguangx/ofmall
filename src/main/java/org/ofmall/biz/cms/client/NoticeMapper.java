package org.ofmall.biz.cms.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.cms.model.Notice;
import org.ofmall.biz.cms.model.NoticeExample;

public interface NoticeMapper extends BaseMapper<Notice, NoticeExample> {
    int countByExample(NoticeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Notice record);

    int insertSelective(Notice record);

    List<Notice> selectByExample(NoticeExample example);

    Notice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);
}