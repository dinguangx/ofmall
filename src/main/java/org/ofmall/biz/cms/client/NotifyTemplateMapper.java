package org.ofmall.biz.cms.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.cms.model.NotifyTemplate;
import org.ofmall.biz.cms.model.NotifyTemplateExample;

public interface NotifyTemplateMapper extends BaseMapper<NotifyTemplate, NotifyTemplateExample> {
    int countByExample(NotifyTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(NotifyTemplate record);

    int insertSelective(NotifyTemplate record);

    List<NotifyTemplate> selectByExample(NotifyTemplateExample example);

    NotifyTemplate selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(NotifyTemplate record);

    int updateByPrimaryKey(NotifyTemplate record);
}