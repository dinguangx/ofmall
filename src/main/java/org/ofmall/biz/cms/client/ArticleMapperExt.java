package org.ofmall.biz.cms.client;

import org.ofmall.biz.cms.bean.ArticleBean;

import java.util.List;

public interface ArticleMapperExt  {
    List<ArticleBean> selectArticleBeanList(ArticleBean example);

}