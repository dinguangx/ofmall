package org.ofmall.biz.cms.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.cms.model.FriendLink;
import org.ofmall.biz.cms.model.FriendLinkExample;

public interface FriendLinkMapper extends BaseMapper<FriendLink, FriendLinkExample> {
    int countByExample(FriendLinkExample example);

    int deleteByPrimaryKey(Long id);

    int insert(FriendLink record);

    int insertSelective(FriendLink record);

    List<FriendLink> selectByExample(FriendLinkExample example);

    FriendLink selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FriendLink record);

    int updateByPrimaryKey(FriendLink record);
}