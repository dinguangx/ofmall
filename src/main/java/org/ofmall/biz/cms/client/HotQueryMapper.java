package org.ofmall.biz.cms.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.cms.model.HotQuery;
import org.ofmall.biz.cms.model.HotQueryExample;

public interface HotQueryMapper extends BaseMapper<HotQuery, HotQueryExample> {
    int countByExample(HotQueryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(HotQuery record);

    int insertSelective(HotQuery record);

    List<HotQuery> selectByExample(HotQueryExample example);

    HotQuery selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(HotQuery record);

    int updateByPrimaryKey(HotQuery record);
}