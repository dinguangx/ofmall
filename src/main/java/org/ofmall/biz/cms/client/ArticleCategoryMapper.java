package org.ofmall.biz.cms.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.cms.model.ArticleCategory;
import org.ofmall.biz.cms.model.ArticleCategoryExample;

public interface ArticleCategoryMapper extends BaseMapper<ArticleCategory, ArticleCategoryExample> {
    int countByExample(ArticleCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ArticleCategory record);

    int insertSelective(ArticleCategory record);

    List<ArticleCategory> selectByExample(ArticleCategoryExample example);

    ArticleCategory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ArticleCategory record);

    int updateByPrimaryKey(ArticleCategory record);
}