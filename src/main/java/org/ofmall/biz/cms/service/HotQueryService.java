package org.ofmall.biz.cms.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.cms.client.HotQueryMapper;
import org.ofmall.biz.cms.model.HotQuery;
import org.ofmall.biz.cms.model.HotQueryExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yue
 * 2016年3月5日 下午3:45:39
 */
@Service
public class HotQueryService extends BaseService<HotQuery, HotQueryExample> {
    @Autowired
    private HotQueryMapper hotQueryMapper;
    
    @Override
    protected BaseMapper<HotQuery, HotQueryExample> getMapper() {
        return hotQueryMapper;
    }
    
    
}
