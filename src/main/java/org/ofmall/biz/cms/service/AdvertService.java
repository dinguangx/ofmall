package org.ofmall.biz.cms.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.cms.client.AdvertMapper;
import org.ofmall.biz.cms.model.Advert;
import org.ofmall.biz.cms.model.AdvertExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dinguangx@163.com
 * @date 2015-12-19 00:14
 */
@Service
public class AdvertService extends BaseService<Advert, AdvertExample> {
    @Autowired
    AdvertMapper advertMapper;

    @Override
    protected BaseMapper<Advert, AdvertExample> getMapper() {
        return advertMapper;
    }
    
}
