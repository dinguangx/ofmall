package org.ofmall.biz.cms.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.cms.client.FriendLinkMapper;
import org.ofmall.biz.cms.model.FriendLink;
import org.ofmall.biz.cms.model.FriendLinkExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by machenike on 2016/1/7.
 */
@Service
public class FriendLinkService extends BaseService<FriendLink, FriendLinkExample> {
    @Autowired
    private FriendLinkMapper friendLinkMapper;
    
    @Override
    protected BaseMapper<FriendLink, FriendLinkExample> getMapper() {
        return friendLinkMapper;
    }
    
    
}
