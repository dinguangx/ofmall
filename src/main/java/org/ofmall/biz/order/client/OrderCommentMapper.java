package org.ofmall.biz.order.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.order.model.OrderComment;
import org.ofmall.biz.order.model.OrderCommentExample;

public interface OrderCommentMapper extends BaseMapper<OrderComment, OrderCommentExample> {
    int countByExample(OrderCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderComment record);

    int insertSelective(OrderComment record);

    List<OrderComment> selectByExample(OrderCommentExample example);

    OrderComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderComment record);

    int updateByPrimaryKey(OrderComment record);
}