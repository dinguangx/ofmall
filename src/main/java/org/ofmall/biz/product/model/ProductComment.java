package org.ofmall.biz.product.model;

import java.io.Serializable;
import org.ofmall.biz.base.model.BaseModel;

public class ProductComment extends BaseModel implements Serializable {
    /** 评论内容 */
    private String content;

    /** 星级 */
    private Integer star;

    /** 商品ID */
    private Long productInfoId;

    /** 用户ID */
    private Long memberId;

    private static final long serialVersionUID = 1L;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public Long getProductInfoId() {
        return productInfoId;
    }

    public void setProductInfoId(Long productInfoId) {
        this.productInfoId = productInfoId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }
}