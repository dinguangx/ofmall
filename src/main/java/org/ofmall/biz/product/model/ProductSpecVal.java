package org.ofmall.biz.product.model;

import java.io.Serializable;
import org.ofmall.biz.base.model.BaseModel;

public class ProductSpecVal extends BaseModel implements Serializable {
    /** 商品规格ID */
    private Long specGroupId;

    /** 规格值 */
    private String specValue;

    /** 排序 */
    private Integer ordinal;

    /** 规格名称 */
    private String specName;

    private static final long serialVersionUID = 1L;

    public Long getSpecGroupId() {
        return specGroupId;
    }

    public void setSpecGroupId(Long specGroupId) {
        this.specGroupId = specGroupId;
    }

    public String getSpecValue() {
        return specValue;
    }

    public void setSpecValue(String specValue) {
        this.specValue = specValue == null ? null : specValue.trim();
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName == null ? null : specName.trim();
    }
}