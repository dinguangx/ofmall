package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductCategory;
import org.ofmall.biz.product.model.ProductCategoryExample;

public interface ProductCategoryMapper extends BaseMapper<ProductCategory, ProductCategoryExample> {
    int countByExample(ProductCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductCategory record);

    int insertSelective(ProductCategory record);

    List<ProductCategory> selectByExample(ProductCategoryExample example);

    ProductCategory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductCategory record);

    int updateByPrimaryKey(ProductCategory record);
}