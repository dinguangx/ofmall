package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.Brand;
import org.ofmall.biz.product.model.BrandExample;

public interface BrandMapper extends BaseMapper<Brand, BrandExample> {
    int countByExample(BrandExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Brand record);

    int insertSelective(Brand record);

    List<Brand> selectByExample(BrandExample example);

    Brand selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Brand record);

    int updateByPrimaryKey(Brand record);
}