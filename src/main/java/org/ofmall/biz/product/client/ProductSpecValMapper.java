package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductSpecVal;
import org.ofmall.biz.product.model.ProductSpecValExample;

public interface ProductSpecValMapper extends BaseMapper<ProductSpecVal, ProductSpecValExample> {
    int countByExample(ProductSpecValExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductSpecVal record);

    int insertSelective(ProductSpecVal record);

    List<ProductSpecVal> selectByExample(ProductSpecValExample example);

    ProductSpecVal selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductSpecVal record);

    int updateByPrimaryKey(ProductSpecVal record);
}