package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductComment;
import org.ofmall.biz.product.model.ProductCommentExample;

public interface ProductCommentMapper extends BaseMapper<ProductComment, ProductCommentExample> {
    int countByExample(ProductCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductComment record);

    int insertSelective(ProductComment record);

    List<ProductComment> selectByExample(ProductCommentExample example);

    ProductComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductComment record);

    int updateByPrimaryKey(ProductComment record);
}