package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductInfoSpecLink;
import org.ofmall.biz.product.model.ProductInfoSpecLinkExample;

public interface ProductInfoSpecLinkMapper extends BaseMapper<ProductInfoSpecLink, ProductInfoSpecLinkExample> {
    int countByExample(ProductInfoSpecLinkExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductInfoSpecLink record);

    int insertSelective(ProductInfoSpecLink record);

    List<ProductInfoSpecLink> selectByExample(ProductInfoSpecLinkExample example);

    ProductInfoSpecLink selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductInfoSpecLink record);

    int updateByPrimaryKey(ProductInfoSpecLink record);
}