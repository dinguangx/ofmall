package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductAttr;
import org.ofmall.biz.product.model.ProductAttrExample;

public interface ProductAttrMapper extends BaseMapper<ProductAttr, ProductAttrExample> {
    int countByExample(ProductAttrExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductAttr record);

    int insertSelective(ProductAttr record);

    List<ProductAttr> selectByExample(ProductAttrExample example);

    ProductAttr selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductAttr record);

    int updateByPrimaryKey(ProductAttr record);
}