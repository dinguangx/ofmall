package org.ofmall.biz.product.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.product.model.ProductAttrLink;
import org.ofmall.biz.product.model.ProductAttrLinkExample;

public interface ProductAttrLinkMapper extends BaseMapper<ProductAttrLink, ProductAttrLinkExample> {
    int countByExample(ProductAttrLinkExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductAttrLink record);

    int insertSelective(ProductAttrLink record);

    List<ProductAttrLink> selectByExample(ProductAttrLinkExample example);

    ProductAttrLink selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductAttrLink record);

    int updateByPrimaryKey(ProductAttrLink record);
}