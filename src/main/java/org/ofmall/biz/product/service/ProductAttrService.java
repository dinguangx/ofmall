package org.ofmall.biz.product.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.product.client.ProductAttrMapper;
import org.ofmall.biz.product.model.ProductAttr;
import org.ofmall.biz.product.model.ProductAttrExample;

@Service
public class ProductAttrService extends BaseService<ProductAttr, ProductAttrExample> {

	@Autowired
	private ProductAttrMapper productAttrMapper;
	
	@Override
	protected BaseMapper<ProductAttr, ProductAttrExample> getMapper() {
		return productAttrMapper;
	}

}
