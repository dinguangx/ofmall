package org.ofmall.biz.product.service;

import org.ofmall.biz.product.model.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.product.client.BrandMapper;
import org.ofmall.biz.product.model.BrandExample;

/**
 *	产品品牌管理Service
 *
 * @author Leolione
 * @email leolione@outlook.com
 * @since V1.0
 */
@Service
public class BrandService extends BaseService<Brand, BrandExample>{
	
	@Autowired
	private BrandMapper brandMapper;
	
	@Override
	protected BaseMapper<Brand, BrandExample> getMapper() {
		return brandMapper;
	}

}
