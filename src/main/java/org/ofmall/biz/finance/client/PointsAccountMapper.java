package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PointsAccount;
import org.ofmall.biz.finance.model.PointsAccountExample;

public interface PointsAccountMapper extends BaseMapper<PointsAccount, PointsAccountExample> {
    int countByExample(PointsAccountExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PointsAccount record);

    int insertSelective(PointsAccount record);

    List<PointsAccount> selectByExample(PointsAccountExample example);

    PointsAccount selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PointsAccount record);

    int updateByPrimaryKey(PointsAccount record);
}