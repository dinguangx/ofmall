package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PaymentRefund;
import org.ofmall.biz.finance.model.PaymentRefundExample;

public interface PaymentRefundMapper extends BaseMapper<PaymentRefund, PaymentRefundExample> {
    int countByExample(PaymentRefundExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PaymentRefund record);

    int insertSelective(PaymentRefund record);

    List<PaymentRefund> selectByExample(PaymentRefundExample example);

    PaymentRefund selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PaymentRefund record);

    int updateByPrimaryKey(PaymentRefund record);
}