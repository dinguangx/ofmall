package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PointsAward;
import org.ofmall.biz.finance.model.PointsAwardExample;

public interface PointsAwardMapper extends BaseMapper<PointsAward, PointsAwardExample> {
    int countByExample(PointsAwardExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PointsAward record);

    int insertSelective(PointsAward record);

    List<PointsAward> selectByExample(PointsAwardExample example);

    PointsAward selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PointsAward record);

    int updateByPrimaryKey(PointsAward record);
}