package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PointsAccountHistory;
import org.ofmall.biz.finance.model.PointsAccountHistoryExample;

public interface PointsAccountHistoryMapper extends BaseMapper<PointsAccountHistory, PointsAccountHistoryExample> {
    int countByExample(PointsAccountHistoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PointsAccountHistory record);

    int insertSelective(PointsAccountHistory record);

    List<PointsAccountHistory> selectByExample(PointsAccountHistoryExample example);

    PointsAccountHistory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PointsAccountHistory record);

    int updateByPrimaryKey(PointsAccountHistory record);
}