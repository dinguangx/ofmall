package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PointsPayment;
import org.ofmall.biz.finance.model.PointsPaymentExample;

public interface PointsPaymentMapper extends BaseMapper<PointsPayment, PointsPaymentExample> {
    int countByExample(PointsPaymentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PointsPayment record);

    int insertSelective(PointsPayment record);

    List<PointsPayment> selectByExample(PointsPaymentExample example);

    PointsPayment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PointsPayment record);

    int updateByPrimaryKey(PointsPayment record);
}