package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.PaymentType;
import org.ofmall.biz.finance.model.PaymentTypeExample;

public interface PaymentTypeMapper extends BaseMapper<PaymentType, PaymentTypeExample> {
    int countByExample(PaymentTypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PaymentType record);

    int insertSelective(PaymentType record);

    List<PaymentType> selectByExample(PaymentTypeExample example);

    PaymentType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PaymentType record);

    int updateByPrimaryKey(PaymentType record);
}