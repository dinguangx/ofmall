package org.ofmall.biz.finance.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.finance.model.Payment;
import org.ofmall.biz.finance.model.PaymentExample;

public interface PaymentMapper extends BaseMapper<Payment, PaymentExample> {
    int countByExample(PaymentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Payment record);

    int insertSelective(Payment record);

    List<Payment> selectByExample(PaymentExample example);

    Payment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Payment record);

    int updateByPrimaryKey(Payment record);
}