package org.ofmall.biz.member.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.member.client.MemberAddressMapper;
import org.ofmall.biz.member.model.MemberAddress;
import org.ofmall.biz.member.model.MemberAddressExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberAddressService extends BaseService<MemberAddress, MemberAddressExample> {

	@Autowired
	private MemberAddressMapper memberAddressMapper;

	@Override
	protected BaseMapper<MemberAddress, MemberAddressExample> getMapper() {
		return memberAddressMapper;
	}

	/**
	 * 检索用户的收货地址
	 * @param memberId
	 * @return
	 */
	public List<MemberAddress> selectMemberAddress(long memberId) {
		MemberAddressExample example = new MemberAddressExample();
		MemberAddressExample.Criteria criteria = example.createCriteria();
		criteria.andMemberIdEqualTo(memberId);

		return memberAddressMapper.selectByExample(example);
	}

	@Override
	public long insert(MemberAddress entity) {
		//TODO 提取省市区信息
		return super.insert(entity);
	}

	@Override
	public int update(MemberAddress entity) {
		//TODO 提取省市区信息
		return super.update(entity);
	}
}
