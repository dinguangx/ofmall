package org.ofmall.biz.member.service;

import java.util.List;

import org.ofmall.biz.member.client.MemberRankMapper;
import org.ofmall.biz.member.model.MemberRank;
import org.ofmall.biz.member.model.MemberRankExample;
import org.ofmall.biz.base.bean.PageBean;
import org.ofmall.biz.base.bean.PageQueryBean;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberRankService extends BaseService<MemberRank, MemberRankExample>{

	@Autowired
	private MemberRankMapper accountRankMapper;

	@Override
	protected BaseMapper<MemberRank, MemberRankExample> getMapper() {
		return accountRankMapper;
	}

	public PageBean<MemberRank> selectPageBean(final MemberRankExample params, PageQueryBean pageQueryBean)
	{
		return executePageQuery(new PageQueryExecutor<MemberRank>() 
		{
			@Override
			public List<MemberRank> executeQuery() {
				return accountRankMapper.selectByExample(params);
			}
		}, pageQueryBean);
	}

}
