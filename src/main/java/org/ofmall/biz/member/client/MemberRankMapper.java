package org.ofmall.biz.member.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.member.model.MemberRank;
import org.ofmall.biz.member.model.MemberRankExample;

public interface MemberRankMapper extends BaseMapper<MemberRank, MemberRankExample> {
    int countByExample(MemberRankExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MemberRank record);

    int insertSelective(MemberRank record);

    List<MemberRank> selectByExample(MemberRankExample example);

    MemberRank selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MemberRank record);

    int updateByPrimaryKey(MemberRank record);
}