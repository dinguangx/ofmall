package org.ofmall.biz.system.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.system.model.SysResource;
import org.ofmall.biz.system.model.SysResourceExample;

public interface SysResourceMapper extends BaseMapper<SysResource, SysResourceExample> {
    int countByExample(SysResourceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysResource record);

    int insertSelective(SysResource record);

    List<SysResource> selectByExample(SysResourceExample example);

    SysResource selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysResource record);

    int updateByPrimaryKey(SysResource record);
}