package org.ofmall.biz.system.client;

import java.util.List;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.system.model.SysPrivilege;
import org.ofmall.biz.system.model.SysPrivilegeExample;

public interface SysPrivilegeMapper extends BaseMapper<SysPrivilege, SysPrivilegeExample> {
    int countByExample(SysPrivilegeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysPrivilege record);

    int insertSelective(SysPrivilege record);

    List<SysPrivilege> selectByExample(SysPrivilegeExample example);

    SysPrivilege selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysPrivilege record);

    int updateByPrimaryKey(SysPrivilege record);
}