package org.ofmall.biz.system.service;

import org.ofmall.biz.system.model.Express;
import org.ofmall.biz.system.model.ExpressExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.system.client.ExpressMapper;

/**
 *	配送方式管理Service
 *
 * @author Leolione
 * @email leolione@outlook.com
 * @since V1.0
 */
@Service
public class ExpressService extends BaseService<Express, ExpressExample> {
	
	@Autowired
	private ExpressMapper expressMapper;
	
	@Override
	protected BaseMapper<Express, ExpressExample> getMapper() {
		return expressMapper;
	}
	
    /**
     * 根据code查找配送方式
     * @param expressCode
     * @return
     */
    public Express selectByCode(String expressCode) {
    	ExpressExample example = new ExpressExample();
    	ExpressExample.Criteria criteria = example.createCriteria();
        criteria.andExpressCodeEqualTo(expressCode);
        return selectUniqueByExample(example);
    }
    
}
