package org.ofmall.biz.system.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.member.model.Member;
import org.ofmall.biz.system.enums.LogType;
import org.ofmall.biz.system.client.SystemLogMapper;
import org.ofmall.biz.system.model.SysUser;
import org.ofmall.biz.system.model.SystemLog;
import org.ofmall.biz.system.model.SystemLogExample;
import org.ofmall.core.util.AddressUtils;
import org.ofmall.web.util.LoginUserHolder;
import org.ofmall.web.util.RequestHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author dinguangx@163.com
 * @date 2015-12-22 23:14
 */
@Service
public class SystemLogService extends BaseService<SystemLog, SystemLogExample> {
    @Autowired
    private SystemLogMapper systemLogMapper;

    @Override
    protected BaseMapper<SystemLog, SystemLogExample> getMapper() {
        return systemLogMapper;
    }

    /**
     */
    @Transactional
    public void newLog(String title, String content, LogType logType) {
        SysUser currentUser = LoginUserHolder.getLoginUser();

        SystemLog systemlog = new SystemLog();
        systemlog.setTitle(title);
        systemlog.setContent(content);
        systemlog.setAccount(currentUser.getUsername());
        systemlog.setLogType(logType);
        systemlog.setCreateTime(new Date());
        systemlog.setUpdateTime(new Date());
        systemlog.setLogTime(new Date());
        systemlog.setLoginIp(AddressUtils.getIp(RequestHolder.getRequest()));

        String address = null;
        if (!systemlog.getLoginIp().equals("127.0.0.1") && !systemlog.getLoginIp().equals("localhost")) {
            //获取指定IP的区域位置
            try {
                address = AddressUtils.getAddresses("ip=" + systemlog.getLoginIp(), "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            systemlog.setLoginArea(address);

        }

        this.insert(systemlog);
    }


    /**
     * 前台日志记录
     */
    @Transactional
    public void newFrontLog(String title, String content, LogType logType) {
        Member member = LoginUserHolder.getLoginMember();

        String account = member == null ? "NOBODY" : member.getUsername();

        SystemLog systemlog = new SystemLog();
        systemlog.setTitle(title);
        systemlog.setContent(content);
        systemlog.setAccount(account);
        systemlog.setLogType(logType);
        systemlog.setCreateTime(new Date());
        systemlog.setUpdateTime(new Date());
        systemlog.setLogTime(new Date());
        systemlog.setLoginIp(AddressUtils.getIp(RequestHolder.getRequest()));

        String address = null;
        if (!systemlog.getLoginIp().equals("127.0.0.1") && !systemlog.getLoginIp().equals("localhost")) {
            //获取指定IP的区域位置
            try {
                address = AddressUtils.getAddresses("ip=" + systemlog.getLoginIp(), "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            systemlog.setLoginArea(address);

        }

        this.insert(systemlog);
    }
}
