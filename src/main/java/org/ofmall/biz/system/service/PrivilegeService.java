package org.ofmall.biz.system.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.system.client.SysPrivilegeMapper;
import org.ofmall.biz.system.model.SysPrivilege;
import org.ofmall.biz.system.model.SysPrivilegeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by carlosxiao on 30/12/2015.
 */

@Service
public class PrivilegeService extends BaseService<SysPrivilege, SysPrivilegeExample>
{
    @Autowired
    private SysPrivilegeMapper sysPrivilegeMapper;

    @Override
    protected BaseMapper<SysPrivilege, SysPrivilegeExample> getMapper() 
    {
        return sysPrivilegeMapper;
    }
}
