package org.ofmall.biz.system.service;

import java.util.List;

import org.ofmall.biz.base.bean.PageBean;
import org.ofmall.biz.base.bean.PageQueryBean;
import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.system.client.KeyValueMapper;
import org.ofmall.biz.system.model.KeyValue;
import org.ofmall.biz.system.model.KeyValueExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ysqin Email: 442800318@qq.com
 */
@Service
public class KeyValueService extends BaseService<KeyValue, KeyValueExample> {

	@Autowired
	private KeyValueMapper keyValueMapper;

	@Override
	protected BaseMapper<KeyValue, KeyValueExample> getMapper() {
		return keyValueMapper;
	}

	public PageBean<KeyValue> selectPageBean(final KeyValueExample params,
											 PageQueryBean pageQueryBean) {
		return executePageQuery(new PageQueryExecutor<KeyValue>() {
			@Override
			public List<KeyValue> executeQuery() {
				return keyValueMapper.selectByExample(params);
			}
		}, pageQueryBean);
	}

}
