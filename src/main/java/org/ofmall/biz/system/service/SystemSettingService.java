package org.ofmall.biz.system.service;

import org.ofmall.biz.base.client.BaseMapper;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.system.client.SystemSettingMapper;
import org.ofmall.biz.system.model.SystemSetting;
import org.ofmall.biz.system.model.SystemSettingExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dinguangx@163.com
 * @date 2015-12-22 23:50
 */
@Service
public class SystemSettingService extends BaseService<SystemSetting, SystemSettingExample> {
    @Autowired
    SystemSettingMapper systemSettingMapper;
    @Override
    protected BaseMapper<SystemSetting, SystemSettingExample> getMapper() {
        return systemSettingMapper;
    }
}
