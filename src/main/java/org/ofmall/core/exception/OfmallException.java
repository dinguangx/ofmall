package org.ofmall.core.exception;

/**
 * @author dinguangx@163.com
 * @date 2015-12-21 23:56
 */
public class OfmallException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OfmallException() {
        super();
    }
    /**
     * @param msg
     */
    public OfmallException(String msg) {
        super(msg);
    }

    public OfmallException(String message, Throwable cause) {
        super(message, cause);
    }

    public OfmallException(Throwable cause) {
        super(cause);
    }
}
