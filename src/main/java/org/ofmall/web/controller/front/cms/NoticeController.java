package org.ofmall.web.controller.front.cms;

import org.ofmall.biz.base.bean.PageBean;
import org.ofmall.biz.base.bean.PageQueryBean;
import org.ofmall.biz.cms.model.Notice;
import org.ofmall.biz.cms.model.NoticeExample;
import org.ofmall.biz.cms.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Dylan.Ding
 * @date 2016-04-04 17-57
 */
@Controller("front.noticeController")
@RequestMapping("cms/notice")
public class NoticeController {

    @Autowired
    NoticeService noticeService;
    @RequestMapping({"index", "index.html"})
    public String index(ModelMap modelMap) {
        PageBean<Notice> pager = noticeService.selectPageList(new NoticeExample(), new PageQueryBean());
        modelMap.addAttribute("pager", pager);
        return "cms/noticeList";
    }


    @RequestMapping({"{noticeId}","{noticeId}.html"})
    public String noticePage(ModelMap modelMap, @PathVariable("noticeId")Long noticeId) {
        Notice notice = noticeService.selectById(noticeId);
        if(notice == null) {
            return "redirect:index";
        }
        //阅读数加1,目前使用即时更新
        noticeService.updateReadCount(notice);
        modelMap.put("notice", notice);
        return "cms/notice";
    }
}
