package org.ofmall.web.controller.manage.cms;

import org.ofmall.biz.base.bean.PageBean;
import org.ofmall.biz.base.bean.PageQueryBean;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.cms.model.Advert;
import org.ofmall.biz.cms.model.AdvertExample;
import org.ofmall.biz.cms.service.AdvertService;
import org.ofmall.web.controller.manage.ManageBaseController;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/manage/cms/advert/")
public class AdvertController extends ManageBaseController<Advert, AdvertExample> {
	  private static final String page_toList = "/manage/cms/advertList";
	    private static final String page_toAdd = "/manage/cms/advertEdit";
	    private static final String page_toEdit = "/manage/cms/advertEdit";

	
	 @Autowired
	    private AdvertService advertService;
	 
	 @Override
	 public BaseService<Advert, AdvertExample> getService() {
		 return advertService;
	 }
	 
	 public AdvertController() {
		 super.page_toEdit = page_toEdit;
		 super.page_toList = page_toList;
		 super.page_toAdd = page_toAdd;
	 }
	 
		@RequestMapping("loadData")
	    @ResponseBody
	    public PageBean<Advert> loadData(Advert advert, PageQueryBean pageQueryBean) {
		  AdvertExample advertExample=new AdvertExample();
		  AdvertExample.Criteria criteria = advertExample.createCriteria();
	        if (StringUtils.isNotBlank(advert.getTitle())) {
	            criteria.andTitleLike( advert.getTitle());
	        }
	        if (StringUtils.isNotBlank(advert.getCode())) {
	            criteria.andCodeLike(advert.getCode());
	        }
	        advertExample.setOrderByClause("id");
			PageBean<Advert> pager = advertService.selectPageList(advertExample, pageQueryBean);
	        return pager;
	    }
		
}
