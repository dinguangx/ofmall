package org.ofmall.web.controller.manage.product;

import org.ofmall.biz.base.bean.PageBean;
import org.ofmall.biz.base.bean.PageQueryBean;
import org.ofmall.biz.base.service.BaseService;
import org.ofmall.biz.product.model.Brand;
import org.ofmall.biz.product.service.BrandService;
import org.ofmall.web.controller.manage.ManageBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.ofmall.biz.product.model.BrandExample;

/**
 *	商品品牌管理Controller
 *
 * @author Leolione
 * @email leolione@outlook.com
 * @since V1.0
 */
@Controller
@RequestMapping("/manage/product/brand")
public class BrandController extends ManageBaseController<Brand, BrandExample> {
	
	@Autowired
	BrandService brandService;
	
	@Override
	public BaseService<Brand, BrandExample> getService() {
		return brandService;
	}
	
	public BrandController()
	{
        super.page_toList = "manage/product/brand/brandList";
        super.page_toEdit = "manage/product/brand/editBrand";
        super.page_toAdd  = "manage/product/brand/editBrand";
	}
	
	@RequestMapping("loadData")
    @ResponseBody
    public PageBean<Brand> loadData(Brand queryParams, PageQueryBean pageQueryBean) {
		BrandExample example = new BrandExample();
        example.setOrderByClause("ordinal asc");
        PageBean<Brand> pager = brandService.selectPageList(example, pageQueryBean);
        return pager;
    }
	
}
