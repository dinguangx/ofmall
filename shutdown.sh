#!/bin/bash

ps -ef | grep jettyRun | awk '{print $2}' | xargs kill -9
