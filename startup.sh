#!/bin/bash

export root_dir=$0/..

export GRADLE_OPTS=" -XX:PermSize=128M -XX:MaxPermSize=256m -Xnoagent -Djava.compiler=NONE -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005"

./gradlew -p $root_dir --no-daemon clean jettyRun -DhttpPort=8080 -DstopPort=8005
